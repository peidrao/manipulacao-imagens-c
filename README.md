<h1> Projeto de Manipulação de Imagem </h1>

<h4>O que o projeto atendeu?</h4>
<ul>
  <li>Uso de arranjos / matrizes.</li>
  <li>Uso de registros (struct).</li>
  <li>Uso de enumeradores (enum).</li>
  <li>Definição de novos tipos de dados através de typedef.</li>
  <li>Uso de alocação dinâmica.</li>
  <li>Pelo menos uma função/procedimento que usa recursão (Não foi implementado).</li>
  <li>Leitura de arquivos.</li>
  <li>Modularização do programa em diferentes arquivos (uso de diferentes arquivos .c e .h, cada um com sua funcionalidade).
    <ul>
      <li><b>edicao.c</b> - Todas as funções desde filtros e outras manipulações que fazem as transformações nas imagens;</li>
      <li><b>edicao.h</b> - Declaração de todas as funções;</li>
      <li><b>main.c</b> - Onde ocorre a leitura da imagem, o menu é dado ao usuário e ocorre a gravação da imagem;</li>
      <li><b>structs.h</b> - Estruturas onde são trabalhados os pixels e as informações de cada imagem como a largura e sua altura, entre outras;</li>
      <li><b>enums.h</b> - Enumeradores de Página (usado na interface GTK) e Edição (usado para enumerar cada uma das edições).</li>
    </ul>
  </li> 
  <li>Definição de um padrão de indentação do código fonte e de nomenclatura das sub-rotinas e variáveis, e a adequação a este padrão.</li>
  <li>Documentação adequada do código-fonte.</li>
</ul>

<h4>Funções que foram feitas em todo o projeto:</h4>

<b>executarEdicao:</b> Recebe o tipo de edição escolhida pelo usuário na interface e seu respectivo nível (caso haja, caso não é passado 0).

<b>alocarMemoria:</b> Alocação da memória em toda a imagem (Precisa melhorar essa descrição).

<b>isppm:</b> Verifica se o nome da imagem ".ppm" e returnando um boolean.

<b>lerImagem:</b> Leitura da imagem.

<b>gravarImagem:</b> Após passar pelo filtro que o usuário selecionou, a imagem será salva.

<b>filtroEscalaCinza:</b> Filtro para aplicar a escala cinza.

<b>ampliar:</b> Realiza a ampliação da resolução da imagem.

<b>reduzir:</b> Realiza a redução da resolução da imagem.

<b>rotacionar:</b> Rotaciona a imagem dado os ângulos de 90°, 180° ou 270°.

<b>filtroMascara:</b> Aplica filtros que possuem mascára 3x3, como Blurring e Sharpening.

<h4>Funcionalidades complementares:</h4>

<b>GTK (Interface em gtk)</b>: O usuário usará uma interface para aplicar os efeitos que deseja em vez de usar o terminal.

<b>filtroBinarizar (Metodo de limimiar por threshold):</b> Determina-se um nível de cinza a partir de qualquer ponto qu for acessado. 

<b>filtronNegativo:</b> Inverte as cores da imagem.

<b>Pixelizar:</b> A média dos três pixeis modificados é aplicado aos pixeis originais.

<b>filtroSobel:</b> Detecta as bordas horizontais e verticais da imagem em escala de cinza, e assim a imagem é transformada de RGB para a escala de cinza e o resultado é uma imagem com contornos brancos.

<b>filtroGaussiano:</b> Filtro que realiza o desfoque da imagem. Pixeis que são similares são desfocados entre si.

<h4>O que não foi feito?</h4>
O projeto atendeu todas as expectativas em relação as funções e filtros que o foi pedido.

<h4>O que seria feito de diferente?</h4>

O projeto poderia fazer o uso do Makefile para facilitar o processo de compilação do projeto.

<h4>Compilação do arquivo:</h4>
Basta digitar no terminal,
 
<blockquote>gcc src/edicao.c src/main.c -o bin/main -lm `pkg-config --cflags --libs gtk+-3.0` -export-dynamic</blockquote>
 
Com isso a inteface em gtk será iniciada automaticamente, contudo é necessário que o usuário tenha o gtk+3.0, e suas respectivas depêndencias do mesmo em sua distribuição, caso o usuário não possua o programa não iniciará, para isso basta que o mesmo faça a intalação em sua distribuição:

Distribuições Debian/Ubuntu:
<blockquote>sudo apt-get install libgtk-3-dev</blockquote>

<h4>Contribuição:</h4>

<b>Pedro Victor:</b>
<ul>
  <li>Modularização;</li>
  <li>structs.h;</li>
  <li>alocarMemoria();</li>
  <li>lerImagem();</li>
  <li>gravarImagem();</li>
  <li>filtroEscalaCinza();</li>
  <li>filtroBinarizar();</li>
  <li>filtroNegativo();</li>
  <li>filtroPixelizar();</li>
  <li>filtroSobel();</li>
  <li>filtroGaussiano().</li>
</ul>

<b>Weslley:<b>
<ul>
  <li>Interface GTK;</li>
  <li>enums.h;</li>
  <li>isppm();</li>
  <li>lerImagem();</li>
  <li>gravarImagem();</li>
  <li>ampliar();</li>
  <li>reduzir();</li>
  <li>rotacionar();</li>
  <li>filtroMascara().</li>
</ul>