#ifndef ENUMS_H
#define ENUMS_H

// Enumerador de todas as Páginas
typedef enum {
  PAGINA_ESCOLHER_IMAGEM, // Página 1, onde o usuário escolhe o caminho da imagem
  PAGINA_ESCOLHER_EDICAO, // Página 2, onde o usuário escolhe qual edição de imagem quer realizar
  PAGINA_CONCLUIDO // Página 3, onde se encerra o programa
} Pagina;

// Enumerador de todos as edições possíveis
typedef enum {
  ESC, // Escala de Cinza
  AMP, // Ampliação
  RED, // Redução
  ROT, // Rotação
  BLU, // Blurring
  SHA, // Sharpening
  DET, // Detecção de Bordas
  THR, // Binarização de Imagem
  NEG, // Negativo
  PIX, // Pixelização
  SOB, // Sobel
  GAU // Gaussiano
} Edicao;

#endif